# Example file for upstream/metadata.
# See https://wiki.debian.org/UpstreamMetadata for more info/fields.
# Below an example based on a github project.

# Bug-Database: https://github.com/<user>/overlayfs-tools/issues
# Bug-Submit: https://github.com/<user>/overlayfs-tools/issues/new
# Changelog: https://github.com/<user>/overlayfs-tools/blob/master/CHANGES
# Documentation: https://github.com/<user>/overlayfs-tools/wiki
# Repository-Browse: https://github.com/<user>/overlayfs-tools
# Repository: https://github.com/<user>/overlayfs-tools.git
