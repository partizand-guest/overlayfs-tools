% fsck.overlay(1) | User Commands
%
% "January 15 2024"

# NAME

fsck.overlay - Utils for overlayfs.

# SYNOPSIS

**fsck.overlay**

# DESCRIPTION

This project provides several tools:

- **fsck.overlay** - is used to check and optionally repair underlying directories of overlay-filesystem.

# OPTIONS

`fsck.overlay` is a separate binary, and has some extra parameters.

Ensure overlay filesystem is not mounted based on directories which need to check.

Run `fsck.overlay` program. Usage:

    fsck.overlay [-o lowerdir=<lowers>,upperdir=<upper>,workdir=<work>] [-pnyvhV]

    Options:
    -o,                       specify underlying directories of overlayfs:
                              multiple lower directories use ':' as separator
    -p,                       automatic repair (no questions)
    -n,                       make no changes to the filesystem
    -y,                       assume "yes" to all questions
    -v, --verbose             print more messages of overlayfs
    -h, --help                display this usage of overlayfs
    -V, --version             display version information

   Example:

    # fsck.overlay -o lowerdir=lower,upperdir=upper,workdir=work

# DIAGNOSTICS

**overlay** provides some return codes, that can be used in scripts:

    Code Diagnostic
    0      No errors
    1      Filesystem errors corrected
    2      System should be rebooted
    4      Filesystem errors left uncorrected
    8      Operational error
    16     Usage or syntax error
    32     Checking canceled by user request
    128    Shared-library error
    
# Why sudo

As [Linux documentation](https://www.kernel.org/doc/Documentation/filesystems/overlayfs.txt) said, 

> A directory is made opaque by setting the xattr "trusted.overlay.opaque" to "y".

However, only users with `CAP_SYS_ADMIN` can read `trusted.*` extended attributes.

# Warnings / limitations

**overlay** binary

- Only works for regular files and directories. Do not use it on OverlayFS with device files, socket files, etc..
- Hard links may be broken (i.e. resulting in duplicated independent files).
- File owner, group and permission bits will be preserved. File timestamps, attributes and extended attributes might be lost. 
- This program only works for OverlayFS with only one lower layer.
- It is recommended to have the OverlayFS unmounted before running this program.  
    
# SEE ALSO

**overlay**(1)

# AUTHOR

Partizand <partizand@gmail.com>
:   Wrote this manpage for the Debian system.

# COPYRIGHT

Copyright © 2007 Partizand

This manual page was written for the Debian system (and may be used by
others).

Permission is granted to copy, distribute and/or modify this document under
the terms of the GNU General Public License, Version 2 or (at your option)
any later version published by the Free Software Foundation.

On Debian systems, the complete text of the GNU General Public License
can be found in /usr/share/common-licenses/GPL.
